const mdpdf = require('mdpdf');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');

const mdOptions = {
  styles: path.join(__dirname, 'style.css'),
  pdf: {
    format: 'A4',
    orientation: 'portrait'
  }
}

function enumerateFiles() {
  const fileNames =  fs.readdirSync(path.join(__dirname, 'plansjer'))

  let inputPaths = [];
  let outputPaths = [];

  fileNames.forEach(filename => {
    if (!_.endsWith(filename, '.md')) {
      return;
    }

    inputPaths.push(path.join(__dirname, 'plansjer', filename));
    outputPaths.push(path.join(__dirname, 'pdf', _.replace(filename, '.md', '.pdf')));
  });

  return {
    inputPaths,
    outputPaths,
  }
}

async function generatePDF(source, destination) {
  const options = _.merge(mdOptions, { source, destination });

  await mdpdf.convert(options).then((pdfPath) => {
    console.log('✨ PDF Opprettet:', pdfPath);
  }).catch((err) => {
    console.error('❌ Oisann:', err);
  });
}

async function main() {
  const paths = enumerateFiles();

  for (let i = paths.inputPaths.length - 1; i >= 0; i--) {
    await generatePDF(paths.inputPaths[i], paths.outputPaths[i]);
  }
}

main();
