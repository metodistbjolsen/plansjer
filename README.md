## Generering

1. installer [node.js](https://nodejs.org/en/download/)
2. kjør:

`npm install`  
`node pdf.js`  

Deretter skal pdf-ene ligge i pdf-mappa

---

For å legge til flere plansjer, bare putt dem i `plansjer`-mappa med filutvidelsen `.md` (for [Markdown](https://www.computerhope.com/jargon/m/markdown.htm))
