# Vedlikehold av orgelet (1 gang per år):

bakplaten er festet med to skruer i nedre kant

<img class="max-height-250" src="../img/vedlikehold_orgel/plansje_1_hvor_apne.jpg">

bruk kun olje som er beregnet for hammond tonehjulsorgel, den kan bestilles hos TK Music Production AS mobil: +47 920 13 829/638 43 330 eventuelt email: post@pan.no

![image](../img/vedlikehold_orgel/plansje_2_olje.jpg)

Det er tre punkter som skal oljes. Vi kaller dem A, B, C og de sitter her:

<div class="side-om-side">
  <img src="../img/vedlikehold_orgel/plansje_3_fyllingspunkt.jpg">
  <img src="../img/vedlikehold_orgel/plansje_4_fyllingspunkt.jpg">
</div>

Punkt A: skal dryppes 10 dråper, slik at det kun ligger litt olje i bunnen på karet.

NB: ikke overfyll

Punkt B og C: skal hurtig fylles helt opp, la det trekke ned, og fyll helt opp igjen. også her er det viktig å ikke overfylle

ved montering av bakplaten pass på at den treffer sporet i toppen
