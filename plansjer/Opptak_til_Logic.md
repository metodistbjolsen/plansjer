# Opptak til Logic Pro X

Åpne logic og velg inputkilde. Opprett så mange tracks du trenger. Se guide for dante og presonus nedenfor for å koble de til.

Nå man gjør ett opptak så er det tryggest å gi datamaskinen størst rom for latency. Vi gjør det for at datamaskin skal klare å ta opptak på over 1 time. For å gjøre det, åpne Logic gå til instillinger `CMD + ,`, fane 2 `Audio`, `I/O buffer size`, øk den til maks (1024) og øk `Recording delay` til maks også. På Studio macen så er det best å ha *300GB* ledig så disken ikke trenger å fragmentere klippet. Lagre prosjekte før man starter opptaket.

Om maskinen går i dvale så stopper opptaket. På macen så kan du bruke ett lite program som heter caffeine til å holde maskinen på. Alternativt, gå inn i mac instillinger å skru av skjermsparing.

## Dante med Allen & Heath

For å sette opp dante trenger man Dante controller. I Dante controller får man opp alle inputene og alle outputene på nettverket. For at mikseren skal komme opp så må datamaskinen være kablet med ethernet(internett) fra dante kortet til datamaskinen.

For å kunne bruke inputkilde så må man installere Dante Virtual Sound Card. Pass på at mikseren er koblet inn og at Dante er konfigurert. Start Dante Virtual Sound Card og start driveren.

Deretter burde Dante komme frem i Logic Pro. Åpne Logic, gå til instillinger `CMD + ,`, fane 2 `Audio`, `Devices` og Endre `Input Device`  til `Dante`. Når det er gjort så kan man opprette tracks i Logic og begynne å velge input kilder.

## Presonus

Når man skal opp fra presonus mikseren så må man ha en datamaskin med Presonus universal control og Logic. Dataen må ha en firewire port eller så har Simon brukt en firewire til thunderbolt adapter. Deretter er det bare å åpne Logic og velge Presonus som input.
