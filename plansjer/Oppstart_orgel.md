# Oppstart av orgelet:
  - Sett inn stikkontaktene til orgelet og til leslien (2 stk)
  - Hold 'start' til du hører motoren flate ut (ca 12 sekunder)
  - Så slipp 'start' samtidig som du slår på 'run'
  - Vent 20 sekunder før du begynner å spille (oppvarming av rørene)

# Nedstenging av orgelet:
  - Slå av 'run'
  - lukk orgelet
  - trekk ut stikkontaktene til orgelet og til leslien

![image](../img/oppstart_orgel/av_og_paa_knapp.jpg)
