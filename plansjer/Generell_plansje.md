## Generelt
### forberedelse:
  - skru på Cestroen - 'Gudstjeneste med lydmikser' hvis den store miksepulten skal brukes 'Gudstjeneste' ellers

### rydding:
  - skru av Cestroen - lysene skal stå på 'auto'
  - lås døra når du går

## Lyd
### forberedelse:
  - sjekk at teleslynga er på
  - finne og instruere tekstleser i bruk av mikrofon
  - sett nye batterier i myggene (de trådløse mikrofonene)
  - test mikrofonene som skal brukes
  - sjekk at mygg er på

### rydding:
  - rydd bort mikrofoner, mikrofonstativer, di-bokser og
  - eventuelt annet utstyr som har vært i bruk
  - skru av lydmikseren
    home > 'power off' > vent til 'it is now safe' > skru av bryteren på baksida

## Video
### forberedelse:
  - skru på projektorene og TV-skjermen
  - vis frem første lysbilde

### rydding:
  - skru av projektorene og TV-skjermen

## Lys
### forberedelse:
  - koble i strøm til lysene
  - koble om fra cestroen til lysmikseren

### rydding:
  - koble lysene tilbake til cestroen
  - koble ut strøm fra lysene

